"use strict";
/**
 * File: basico.ts
 * Project: curso-typescript-cod3r
 * File Created: Wednesday, 23 September 2020 12:19:49
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Wednesday, 14 October 2020 13:19:03
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2019 - 2020 All rights reserved, Marcant Tecnologia
 * -----
 */
const a = 'Teste TS!';
console.log(a);
//# sourceMappingURL=basico.js.map