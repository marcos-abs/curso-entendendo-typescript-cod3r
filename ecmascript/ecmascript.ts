/**
 * *****
 * File: ecmascript.ts
 * Project: curso-typescript-cod3r
 * File Created: Thursday, 03 June 2021 17:35:03
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Thursday, 03 June 2021 17:55:43
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2019 - 2021 All rights reserved, Marcant Tecnologia
 * -----
 * Description:
 *                Atualizações da linguagem ES6
 * *****
 */

// definição de escopo de alcance da variável com "Let" e "Var"
// for (let i = 0; i < 10; i++) {
//   console.log("i: ", i);
// }
//console.log("i: ", i);

// Arrow Function
// function somar(n1: number, n2: number): number {
//   return n1 + n2;
// }
// console.log(somar(2, 2));
const subtrair = (n1: number, n2: number): number => n1 - n2;
console.log(subtrair(2, 3));
