/**
 * File: tipos.ts
 * Project: curso-typescript-cod3r
 * File Created: Wednesday, 23 September 2020 13:57:32
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Friday, 28 May 2021 00:38:49
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright 2019 - 2020 All rights reserved, Marcant Tecnologia
 */

console.log("Tipos explícitos:");
// tipos explícitos
let minhaIdade: any; // assumir qualquer possibilidade explícitamente.
console.log("minhaIdade=" + typeof minhaIdade);
let minhaIdade2: number = 27; // assumir que seja uma variável numérica explícitamente. // caso retire essa linha ocorrerá um erro de variável não inicializada.
console.log("minhaIdade2=" + typeof minhaIdade2);

// String
let nome: string = "João2";
console.log("nome: " + nome);
// nome = 28; // erro de tipagem, pois já havia inicializado (inferido) o tipo string.

// numbers
let idade: number = 27;
// idade = 'Ana' // Erro de tipagem, idem ao anterior.
idade = 27.5;
console.log("idade: " + idade);

// boolean
let possuiHobbies: boolean = false;
// possuiHobbies = 1  // Erro de tipagem, idem ao anterior.
console.log("possuiHobbies: " + possuiHobbies);

// array
let hobbies: any[] = ["Cozinhar", "Praticar Esportes", 10]; // explicitamente um array de qualquer tipo.
// let hobbies: string[] = ["Cozinhar", "Praticar Esportes"]; // explicitamente um array de strings, ou seja, não armazena outro tipo senão strings.
console.log("Tipo: " + typeof hobbies); // aparecerá como object
console.log("Hobbies: " + hobbies[0] + "; " + hobbies[1]);
hobbies = [100, 200, 300];
console.log("Números: " + hobbies);

// tuplas
let endereco: [string, number] = ["Av. Principal", 99];
console.log(endereco);

let endereco2: [string, number, string] = ["Rua Importante", 1260, ""];
console.log(endereco2);

endereco2 = ["Rua Importante", 1260, "Bloco C"];
console.log(endereco2);

// enums
enum Cor {
  Cinza, // 0
  Verde = 100, // 100
  Azul = 10, // 10
  Laranja, // 11
  Amarelo, // 12
  Vermelho = 100, // 100
}

let minhaCor1: Cor = Cor.Verde;
let minhaCor2: Cor = Cor.Vermelho;
console.log("minha cor: " + minhaCor1 + minhaCor2); // aparecerá 100100

// any ---> auxilia no processo de migração de JS para TypeScript, por conta de manter o tipo como no JS.
let carro: any = "BMW";
console.log(carro);

carro = { marca: "BMW", ano: 2019 };
console.log(carro);

// funções
function retornaMeuNome(): string {
  // explicitamente tipado.
  return nome;
}

console.log(retornaMeuNome());

function digaOi(): void {
  // explicitamente tipado.
  console.log("Oi!");
}

digaOi();

function multiplicar(numA: any, numB: any): number {
  return numA * numB;
}

console.log(multiplicar(2, "Bia")); //NaN => Not a number

function multiplicar2(numA: number, numB: number): number {
  return numA * numB;
}

console.log(multiplicar(4.7, 9)); // 42.300000000000004 (!!)

const teste = function (a: number, b: number): boolean {
  return a > b;
};

console.log("teste(2,1);: ", teste(2, 1));

// tipo função

let calculo: (x: number, y: number) => number; // let calculo: any; // também funciona.

calculo = multiplicar;

console.log("calculo: ", calculo(5, 6));

// tipo objeto
let usuario: { nome: string; idade: number } = {
  // explicitamente tipado.
  nome: "João",
  idade: 27,
};

console.log("usuário: ", usuario);

usuario = {
  // mesmo em ordem diferente é considerado correto.
  idade: 31,
  nome: "Maria",
};

console.log("usuario: ", usuario);

// Desafio
/**
 * Criar um objeto funcionário com:
 *     - Array de strings com os nomes dos supervisores
 *     - Função de bater o ponto que recebe a hora (número)
 *         e retorna uma string.
 *             -> Ponto normal ( <= 8 )
 *             -> Fora do horário ( > 8 )
 */

let funcionario: {
  supervisores: string[];
  baterPonto: (horas: number) => string;
} = {
  supervisores: ["Ana", "Fernando"],
  baterPonto(horario: number): string {
    if (horario <= 8) {
      return "Ponto normal";
    } else {
      return "Fora do horário!";
    }
  },
};
console.log("funcionario.supervisores: ", funcionario.supervisores);
console.log("funcionario.baterPonto(8): ", funcionario.baterPonto(8));
console.log("funcionario.baterPonto(9): ", funcionario.baterPonto(9));

// Alias
type Funcionario = {
  supervisores: string[];
  baterPonto: (horas: number) => string;
};

let funcionario2: Funcionario = {
  supervisores: ["Bia", "Carlos"],
  baterPonto(horario: number): string {
    if (horario <= 8) {
      return "Ponto normal";
    } else {
      return "Fora do horário!";
    }
  },
};

console.log("funcionario2.supervisores: ", funcionario2.supervisores);

// Union Types
let nota: number | string = 10;
console.log(`Minha nota é ${nota}!`);
nota = "10";
console.log(`Minha nota é ${nota}!`);

// Checando tipos
let valor = "30";

if (typeof valor === "number") {
  // "number" não é a mesma coisa que number (sem aspas).
  console.log("É um number!");
} else {
  console.log("É um", typeof valor);
}

// tipo never
function falha(msg: string): never {
  throw new Error(msg);
}

const produto = {
  nome: "Sabão", // nome: '     '
  preco: 8, // preco: -1,
  validaProduto() {
    if (!this.nome || this.nome.trim().length == 0) {
      falha("Precisa ter um nome");
    }
    if (this.preco <= 0) {
      falha("Preco inválido!");
    }
  },
};

produto.validaProduto();

let altura = 12;
//altura = null // erro de compatibilidade de tipos do typescript. Caso queira que isso não ocorra, existe a opção de desabilitá-lo no "tsconfig.json", chama-se "strictNullChecks" que por padrão é "true", bastando somente mudá-lo para "false".

let alturaOpcional: null | number = 12;
alturaOpcional = null; // erro resolvido com o "|" ou "union".

type Contato = {
  nome: string;
  tel1: string;
  tel2: string | null;
};

const contato1: Contato = {
  nome: "Fulano",
  tel1: "98765432",
  tel2: null, // não dá erro por conta do union.
};

console.log("contato1.nome: ", contato1.nome);
console.log("contato1.tel1: ", contato1.tel1);
console.log("contato1.tel2: ", contato1.tel2);

let podeSerNulo = null; // variável definida como "any".
console.log("typeof(podeSerNulo): ", typeof podeSerNulo); //  Correção definida como "object" e não "any".
podeSerNulo = 12;
console.log("podeSerNulo: ", podeSerNulo);
console.log("typeof(podeSerNulo): ", typeof podeSerNulo); //  redefinida para "number".
podeSerNulo = "abc"; // não dá erro por conta do tipo "any".  Correção definida como "object" e não "any".
console.log("podeSerNulo: ", podeSerNulo);
console.log("typeof(podeSerNulo): ", typeof podeSerNulo); //  redefinida para "string".

// Desafio
type ContaBancaria = {
  saldo: number;
  depositar: (valor: number) => void;
};

let contaBancaria = {
  saldo: 3456,
  depositar(valor: number) {
    this.saldo += valor;
  },
};

type Correntista = {
  nome: string;
  contaBancaria: ContaBancaria;
  contatos: string[];
};

let correntista = {
  nome: "Ana Silva",
  contaBancaria: contaBancaria,
  contatos: ["3456890", "98765432"],
};

correntista.contaBancaria.depositar(3000);
console.log("correntista: ", correntista);
