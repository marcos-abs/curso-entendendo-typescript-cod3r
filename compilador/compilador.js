"use strict";
/**
 * *****
 * File: compilador.ts
 * Project: curso-typescript-cod3r
 * File Created: Friday, 28 May 2021 00:37:38
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Friday, 28 May 2021 09:27:08
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2019 - 2021 All rights reserved, Marcant Tecnologia
 * -----
 * Description:
 *
 * *****
 */
let canal = "Gaveta";
let inscritos = 610234;
// canal = inscritos;
console.log("canal: ", canal);
//let nome = 'Pedro'; // não podem haver nomes repetidos no mesmo contexto (já existe a declaração "nome" no arquivo "tipos.ts").
function soma(a, b) {
    return a + b;
}
let qualquerCoisa;
qualquerCoisa = 12;
qualquerCoisa = "abc"; // Por quê? rs - Quando o Typescript "consegue" mapear o tipo necessário para que funcione, ele tentará resolver sem alarme, contudo não é uma boa prática.
// function saudar(isManha: boolean): string | null {
function saudar(isManha, horas) {
    let caminho;
    let saudacao;
    if (isManha) {
        saudacao = "Bom dia!";
    }
    else {
        saudacao = "Tenha uma boa vida!"; // sem a atribuição da saudacao em somente um condição, causando a atribuição nulo, o compilador registrará como erro e não gerará o código Typescript.
    }
    return saudacao;
}
//# sourceMappingURL=compilador.js.map